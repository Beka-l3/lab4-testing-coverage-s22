package com.hw.db.controllers;

import com.hw.db.DAO.PostDAO;
import com.hw.db.DAO.ThreadDAO;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class threadTests {

    @Test
    @DisplayName("Passing since and desc -> add DESC")
    void TreeSortTest2() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new ThreadDAO(mockJdbc);
        
        ThreadDAO.treeSort(1, 2, 3, true);
        
        verify(mockJdbc).query( Mockito.eq( "SELECT * FROM \"posts\" WHERE thread = ?  AND branch < (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch DESC  LIMIT ? ;"), Mockito.any(PostDAO.PostMapper.class), Mockito.eq(1), Mockito.eq(3), Mockito.eq(2) );
    }


    @Test
    @DisplayName("Passing since -> compare dates")
    void TreeSortTest1() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new ThreadDAO(mockJdbc);
        
        ThreadDAO.treeSort(1, 2, 3, null);
        
        verify(mockJdbc).query( Mockito.eq("SELECT * FROM \"posts\" WHERE thread = ?  AND branch > (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch LIMIT ? ;"), Mockito.any(PostDAO.PostMapper.class), Mockito.eq(1), Mockito.eq(3), Mockito.eq(2) );
    }



}
