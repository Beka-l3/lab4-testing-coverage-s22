package com.hw.db.controllers;


import com.hw.db.DAO.ForumDAO;
import com.hw.db.DAO.UserDAO;
import org.junit.jupiter.api.DisplayName;
import org.mockito.Mockito;
import org.junit.jupiter.api.Test;
import org.springframework.jdbc.core.JdbcTemplate;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;


public class forumTests {

    @Test
    @DisplayName("Passing null, null, null")
    void UserTest1() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new ForumDAO(mockJdbc);

        ForumDAO.UserList("slug", null, null, null);

        verify(mockJdbc).query(Mockito.eq( "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname;"), Mockito.eq(new Object[] { "slug" }), Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    @DisplayName("Passing limit, since, false")
    void UserTest2() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        ForumDAO forumDAO = new ForumDAO(mockJdbc);
        
        ForumDAO.UserList("slug", 1, "since", false);
        
        verify(mockJdbc).query(Mockito.eq( "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname > (?)::citext ORDER BY nickname LIMIT ?;"), Mockito.eq(new Object[] { "slug", "since", 1}), Mockito.any(UserDAO.UserMapper.class) );
    }
    
    @Test
    @DisplayName("Passing limit, null, true")
    void UserTest3() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new ForumDAO(mockJdbc);
        
        ForumDAO.UserList("slug", 1, null, true);
        
        verify(mockJdbc).query(Mockito.eq( "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname desc LIMIT ?;"), Mockito.eq(new Object[] { "slug", 1}), Mockito.any(UserDAO.UserMapper.class));
    }


    @Test
    @DisplayName("Passing null, null, true")
    void UserTest4() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new ForumDAO(mockJdbc);
        
        ForumDAO.UserList("slug", null, null, true);
        
        verify(mockJdbc).query(Mockito.eq( "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname desc;"), Mockito.eq(new Object[] { "slug" }), Mockito.any(UserDAO.UserMapper.class));
    }



    @Test
    @DisplayName("Passing limit, null, false")
    void UserTest5() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new ForumDAO(mockJdbc);
        
        ForumDAO.UserList("slug", 1, null, false);
        
        verify(mockJdbc).query(Mockito.eq( "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname LIMIT ?;"), Mockito.eq(new Object[] { "slug", 1 }), Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    @DisplayName("Passing null, since, false")
    void UserTest6() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new ForumDAO(mockJdbc);
        
        ForumDAO.UserList("slug", null, "since", false);
        
        verify(mockJdbc).query(Mockito.eq( "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname > (?)::citext ORDER BY nickname;"), Mockito.eq(new Object[] { "slug", "since" }), Mockito.any(UserDAO.UserMapper.class));
    }

}
