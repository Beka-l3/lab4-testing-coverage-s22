package com.hw.db.controllers;

import com.hw.db.DAO.UserDAO;
import com.hw.db.models.User;
import org.junit.jupiter.api.DisplayName;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;
import org.junit.jupiter.api.Test;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.never;



public class userTests {
    @Test
    @DisplayName("Nothing changed")
    void ChangeTest1() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new UserDAO(mockJdbc);

        UserDAO.Change(new User("user", null, null, null));

        verify( mockJdbc, never()).update(Mockito.any(String.class), Mockito.any(Object[].class) );
    }

    @Test
    @DisplayName("All fields changed")
    void ChangeTest2() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new UserDAO(mockJdbc);

        UserDAO.Change(new User("user", "mail@innopolis", "Name Surename", "info"));
        verify( mockJdbc, never()).update(Mockito.any(String.class), Mockito.any(Object[].class) );

        verify(mockJdbc).update( Mockito.eq("UPDATE \"users\" SET  email=? , fullname=? , about=?  WHERE nickname=?::CITEXT;"), Mockito.eq("mail@innopolis"), Mockito.eq("Name Surename"), Mockito.eq("info"), Mockito.eq("user") );
    }
    @Test
    @DisplayName("Change email")
    void ChangeTest3() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new UserDAO(mockJdbc);

        UserDAO.Change(new User("user", "mail@innopolis", null, null));
        verify( mockJdbc, never()).update(Mockito.any(String.class), Mockito.any(Object[].class) );

        verify(mockJdbc).update( Mockito.eq("UPDATE \"users\" SET  email=?  WHERE nickname=?::CITEXT;"), Mockito.eq("mail@innopolis"), Mockito.eq("user") );
    }

    @Test
    @DisplayName("Change fullname")
    void ChangeTest4() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new UserDAO(mockJdbc);

        UserDAO.Change(new User("user", null, "User Name", null));
        verify(mockJdbc, never()).update(Mockito.any(String.class), Mockito.any(Object[].class) );

        verify(mockJdbc).update( Mockito.eq("UPDATE \"users\" SET  fullname=?  WHERE nickname=?::CITEXT;"), Mockito.eq("User Name"), Mockito.eq("user") );
    }

    @Test
    @DisplayName("Change about")
    void ChangeTest5() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new UserDAO(mockJdbc);

        UserDAO.Change(new User("user", null, null, "info"));
        verify( mockJdbc, never()).update(Mockito.any(String.class), Mockito.any(Object[].class) );

        verify(mockJdbc).update( Mockito.eq("UPDATE \"users\" SET  about=?  WHERE nickname=?::CITEXT;"), Mockito.eq("info"), Mockito.eq("user") );
    }
}
