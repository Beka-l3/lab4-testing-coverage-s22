package com.hw.db.controllers;

import com.hw.db.DAO.PostDAO;
import com.hw.db.models.Post;
import com.hw.db.DAO.UserDAO;
import org.junit.jupiter.api.DisplayName;
import org.springframework.jdbc.core.JdbcTemplate;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import static org.mockito.Mockito.*;

import java.sql.Timestamp;


public class postTests {

    @Test
    @DisplayName("Change all fields")
    void SetPostTest1() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new PostDAO(mockJdbc);

        Post oldPost = new Post();
        oldPost.setAuthor("user");
        oldPost.setMessage("this is old post");
        oldPost.setCreated(new Timestamp(0));

        Mockito.when(mockJdbc.queryForObject( Mockito.anyString(), Mockito.any(PostDAO.PostMapper.class), Mockito.anyInt() )).thenReturn(oldPost);

        Post newPost = new Post();
        newPost.setMessage("this is new post");
        newPost.setCreated(new Timestamp(1));
        newPost.setAuthor("new user");

        PostDAO.setPost(0, newPost);
        
        verify(mockJdbc).update( Mockito.eq("UPDATE \"posts\" SET  author=?  ,  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"), Mockito.eq("new user"), Mockito.eq("this is new post"), Mockito.eq(new Timestamp(1)), Mockito.eq(0) );
    }

    @Test
    @DisplayName("Change message field")
    void SetPostTest2() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new PostDAO(mockJdbc);

        Post oldPost = new Post();
        oldPost.setAuthor("user");
        oldPost.setMessage("this is old post");
        oldPost.setCreated(new Timestamp(0));
        
        Mockito.when(mockJdbc.queryForObject( Mockito.anyString(), Mockito.any(PostDAO.PostMapper.class), Mockito.anyInt() )).thenReturn(oldPost);

        Post newPost = new Post();
        newPost.setMessage("this is new post");
        PostDAO.setPost(0, newPost);
        verify(mockJdbc).update( Mockito.eq("UPDATE \"posts\" SET  message=?  , isEdited=true WHERE id=?;"), Mockito.eq("this is new post"), Mockito.eq(0) );
    }

    @Test
    @DisplayName("Change created field")
    void SetPostTest3() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new PostDAO(mockJdbc);

        Post oldPost = new Post();
        oldPost.setAuthor("user");
        oldPost.setMessage("this is old post");
        oldPost.setCreated(new Timestamp(0));

        Mockito.when(mockJdbc.queryForObject( Mockito.anyString(), Mockito.any(PostDAO.PostMapper.class), Mockito.anyInt() )).thenReturn(oldPost);

        Post newPost = new Post();
        newPost.setCreated(new Timestamp(1));
        PostDAO.setPost(0, newPost);
        
        verify(mockJdbc).update( Mockito.eq("UPDATE \"posts\" SET  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"), Mockito.eq(new Timestamp(1)), Mockito.eq(0) );
    }

    @Test
    @DisplayName("Change nothing")
    void SetPostTest4() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new PostDAO(mockJdbc);

        PostDAO.setPost(0, new Post());

        verify( mockJdbc, never()).update(Mockito.anyString(), Mockito.<Object>any() );
    }
}
